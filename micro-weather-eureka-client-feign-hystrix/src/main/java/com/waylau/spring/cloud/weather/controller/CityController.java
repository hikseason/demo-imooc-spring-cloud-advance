package com.waylau.spring.cloud.weather.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.waylau.spring.cloud.weather.service.CityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangzhanjin on 2018/4/12.
 */
@RestController
public class CityController {

    @Autowired
    private CityClient cityClient;


    @RequestMapping("/cities")
    @HystrixCommand(fallbackMethod = "defaultCities")
    public String listCity(){
        //通过Feign客户端来查找，如果它服务过载了可以启用断路器
        String body = cityClient.listCity();
        return body;
    }

    public String defaultCities(){
        return "City Dta Server is down!";
    }
}
