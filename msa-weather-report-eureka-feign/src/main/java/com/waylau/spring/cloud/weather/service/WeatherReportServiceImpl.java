package com.waylau.spring.cloud.weather.service;

import com.waylau.spring.cloud.weather.vo.Forecast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.waylau.spring.cloud.weather.vo.Weather;
import com.waylau.spring.cloud.weather.vo.WeatherResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Weather Report Service.
 *
 * @author <a href="https://waylau.com">Way Lau</a>
 * @since 1.0.0 2017年11月24日
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {


    @Autowired
    private WeatherDataClient weatherDataClient;

    @Override
    public Weather getDataByCityId(String cityId) {
        //由天气数据API微服务来提供
        WeatherResponse resp = weatherDataClient.getDataByCityId(cityId);
        Weather data = resp.getData();
        return data;
    }

}
