package com.waylau.spring.cloud.weather.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by wangzhanjin on 2018/4/15.
 */
@FeignClient("msa-weather-city-eureka")
public interface CityClient {
    @GetMapping("cities")
    String listCity();
}
