package com.waylau.spring.cloud.weather.controller;

import com.waylau.spring.cloud.weather.service.CityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangzhanjin on 2018/4/12.
 */
@RestController
public class CityController {

    @Autowired
    private CityClient cityClient;

    @RequestMapping("/cities")
    public String listCity(){
        //通过Feign客户端来查找
        String body = cityClient.listCity();
        return body;
    }
}
