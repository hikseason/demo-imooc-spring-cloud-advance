package com.waylau.spring.cloud.weather.service;

/**
 * WeatherDataCollectionService
 * 数采集服务
 * Created by wangzhanjin on 2018/4/15.
 */
public interface WeatherDataCollectionService {

    /**
     * 根据城市Id同步天气
     * @param cityId
     */
    void sysncDataByCityId(String cityId);
}
