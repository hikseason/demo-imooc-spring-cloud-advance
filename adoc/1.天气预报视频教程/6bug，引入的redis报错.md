## 1.修改pom
1. 修改如下,但后来发现不是这个错
```
<dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-redis</artifactId>
        <exclusions>
            <exclusion>
                <groupId>io.lettuce</groupId>
                <artifactId>lettuce-core</artifactId>
            </exclusion>
```

2. 百度搜索：io.lettuce springboot报错
3. 关于Redis部分，springboot支持jedis和lettuce。但是默认配置的是letuce。     
   如果要使用jedis方式处理redis, Eclipse不需要做任何特殊配置就能使用，但是intellij需要特殊配置，官方参考手册也提示需要特殊配置。
4. 地址：https://my.oschina.net/u/3651261/blog/1549303

## 2.解决不了
1. 只能退一个版本，但是没有quartz了
2. 疯了要

## 3.找到maven仓库
1. 删除jar包，C:\Users\wangzhanjin\.m2\repository\io\lettuce