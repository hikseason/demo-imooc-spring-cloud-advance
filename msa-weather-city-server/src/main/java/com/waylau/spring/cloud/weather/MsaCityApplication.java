package com.waylau.spring.cloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsaCityApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsaCityApplication.class, args);
	}
}
