package com.waylau.spring.cloud.weather.service;

import com.waylau.spring.cloud.weather.vo.Forecast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.waylau.spring.cloud.weather.vo.Weather;
import com.waylau.spring.cloud.weather.vo.WeatherResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Weather Report Service.
 *
 * @author <a href="https://waylau.com">Way Lau</a>
 * @since 1.0.0 2017年11月24日
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {


    @Override
    public Weather getDataByCityId(String cityId) {
        //todo 改为由天气数据API微服务来提供
        Weather data = new Weather();
        data.setAqi("81");
        data.setCity("深圳");
        data.setGanmao("容易感冒");
        data.setWendu("22");

        List<Forecast> forecastList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Forecast forecast = new Forecast();
            forecast.setType("晴");
            forecast.setFengxiang("无风");
            forecast.setHigh("高温33度");
            forecast.setLow("低温10度");

            forecast.setDate(29 + i + "日星期天");
            forecastList.add(forecast);
        }


        data.setForecast(forecastList);

        return data;
    }

}
