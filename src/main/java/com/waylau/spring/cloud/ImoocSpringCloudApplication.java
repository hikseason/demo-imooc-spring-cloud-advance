package com.waylau.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImoocSpringCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImoocSpringCloudApplication.class, args);
	}
}
